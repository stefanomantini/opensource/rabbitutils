import logging
import inquirer
import core_utils

def main():

  core_utils.big_text("Rabbit Utils")
  logging.warning("###################################################################################################")
  logging.warning("This CLI runs various python scripts which interact with the RabbitMQ HTTP & AMQP APIs ")
  logging.warning("###################################################################################################")

  # holds the inquirer questions objects
  questions = []

  # Define question for running scripts
  questions.append(
    inquirer.List(
      'utils',
      message='Select Utility',
      choices=core_utils.list_files_in_dir(cfg["scriptDir"])
    )
  )
  answers = inquirer.prompt(questions)
  logging.info("User selected: " + answers["utils"])
  core_utils.execute_app(cfg["scriptDir"] + "/" + answers["utils"])

if __name__ == '__main__':
  cfg = core_utils.read_config()
  main()

  


