# import system requirements
import requests
import logging
import json

# import package from parent dir
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import core_utils


if __name__ == '__main__':
  # import config
  cfg=core_utils.read_config()
  
  # Build API string from config
  api_root=core_utils.management_api()
  core_utils.big_text("List Users")
  logging.warning("###################################################################################################")
  logging.warning("This CLI will list enrolled users and tags")
  logging.warning("###################################################################################################")

  # make request to endpoint
  r = requests.get(api_root+"/users")
  
  user_names = []
  for d in r.json():
    user = {}
    user["name"] = d["name"]
    user["tags"] = d["tags"]
    user_names.append(user)
    
  logging.info("ENROLLED USERS: ")
  logging.info(core_utils.pretty_print(user_names))

