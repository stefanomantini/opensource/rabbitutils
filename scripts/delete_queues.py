# import system requirements
import requests
import logging
import json
import inquirer

# import package from parent dir
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import core_utils


if __name__ == '__main__':
  # import config
  cfg=core_utils.read_config()

  # Build API string from config
  api_root=core_utils.management_api()
  logging.debug("RabbitMQ management root =" + api_root)
  core_utils.big_text("Delete Queues")
  logging.warning("###################################################################################################")
  logging.warning("Note, this CLI will DELETE entire queues and their contents- they will have to be recreated "
                  "subsequently  messages from a queue into a json file, please be aware that this will halt "
                  "current processing and you will have to manually recreate queues, I hope you know what "
                  "you are doing! :)")
  logging.warning("###################################################################################################")

  # Get list of created vhosts
  vhosts = core_utils.get_vhosts()
  if vhosts == None:
    logging.error("VHOST API ERROR - script terminating")
    exit()
  else:
    logging.debug("VHOSTS" + core_utils.pretty_print(vhosts))

    # get vhost names (handle url encoding)
    vhost_names = []
    for vh in vhosts:
      if vh["name"]=='/':
        vhost_names.append("%2F")
      else:
        vhost_names.append(vh["name"])

    # ask vhost question
    vhost_answers = core_utils.enquire_vhosts(vhost_names)

    # get queues for selected vhost
    logging.error(vhost_answers["vhost"])
    queues = core_utils.get_queues_for_vhost(vhost_answers["vhost"])
    logging.debug(core_utils.pretty_print(queues))

    # get queue names from object
    queue_names=[]
    if len(queues)==0: 
      logging.error("no queues found")
    else:
      for q in queues:
        queue_names.append(q["name"])
      logging.debug(queue_names)
      # prompt for queue deletion selection
      queue_answers = core_utils.enquire_queues(queue_names)
      logging.debug("selection" + str(queue_answers["queues"]))
      
      if len(queue_answers["queues"]) > 0:
        # delete queues 
        for q in queue_answers["queues"]:
          logging.debug("queue"+str(q))
          core_utils.delete_queue(vhost_answers["vhost"], q)
      else:
        logging.error("no selection made")


   
