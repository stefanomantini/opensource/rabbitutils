# Getting started

# Background
Some utils interacting with the RabbitMQ REST API written in python

## Running Locally
Modify the config.json file to include the required connection details 
for your rabbitmq cluster

### Running Locally
* `pip install -r requirements.txt`
* `python main.py`

### Running in docker
* `./build.sh`
* `docker run -it rabbit_utils`