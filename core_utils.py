import json, subprocess, sys, logging
from os import listdir
from os.path import isfile, join
from logging.handlers import RotatingFileHandler
from logging import handlers
import requests
import inquirer
import pika
from pyfiglet import figlet_format

# Read config from static file
def read_config():
  with open('config.json') as f:
    return json.load(f)

# Set log level after reading file
cfg = read_config()

# log configuration
logger = logging.getLogger('')
logger.setLevel(cfg["logLevel"])
fh = logging.FileHandler(cfg["logDir"])
sh = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s|%(name)s|[%(threadName)-12.12s]|[%(levelname)-5.5s]|%(message)s',
                               datefmt='%Y-%m-%d %X')
fh.setFormatter(formatter)
sh.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(sh)
fh = RotatingFileHandler(cfg["logDir"],maxBytes=1000, backupCount=5)


########################## shared functions ##############################

# list files in a directory
def list_files_in_dir(script_path):
  logging.debug("Listing files for directory" + script_path)
  return [f for f in listdir(script_path) if isfile(join(script_path, f))]

# runs a script as a subprocess
def execute_app(path):
  logging.debug("Executing script")
  subprocess.call(["python",path])

def pretty_print(json_data):
  return "Logging JSON Output \n"+json.dumps(
                json_data, 
                sort_keys=False,
                indent=2, 
                separators=(',', ': ')
  )

def management_api():
  cfg = read_config()
  return cfg["protocol"]+cfg["username"]+":"+cfg["password"]+"@"+cfg["host"]+":"+str(cfg["mgmtPort"])+"/api"
api_root = management_api()


def get_vhosts():
  vhost_api = api_root + "/vhosts" # + cfg["vhost"]
  logging.debug("VHOST API endpoint" + vhost_api)
  r = requests.get(vhost_api, verify=False)
  if r.status_code == 200:
    logging.debug("VHost API call successful: "+ str(r.json()))
    return r.json()
  else: 
    logging.error("VHost API call not successful")
    return None

def enquire_vhosts(vhost_names):
  questions = []
  # Define question for running scripts
  questions.append(
        inquirer.List(
        'vhost',
        message='Select VHost',
        choices=vhost_names
      )
  )
  return inquirer.prompt(questions)

def get_queues_for_vhost(vhost_name):
  logging.debug("getting queues for vhost")
  queue_api = api_root + "/queues/" + vhost_name
  logging.debug("Queue API endpoint" + queue_api)
  r = requests.get(queue_api, verify=False)
  if r.status_code == 200:
    logging.debug("Queue API call successful: "+ str(r.json()))
    return r.json()
  else: 
    logging.error("Queue API call not successful")
    return None



def enquire_queues(queue_list):
  logging.debug("Select Queues for deletion")
  questions = [
    inquirer.Checkbox('queues',
                      message="Select queues for deletion",
                      choices=queue_list,
                      ),
  ]
  return inquirer.prompt(questions)

def delete_queue(vhost, queue_name):
  vhost_api = api_root + "/queues/"+vhost+"/"+queue_name
  logging.debug("VHOST API endpoint" + vhost_api)
  r = requests.delete(vhost_api, verify=False)
  if r.status_code == 204:
    logging.info("Seccessfully deleted Queue:"+queue_name)
    return None
  else: 
    logging.error("Failed to delete Queue:"+queue_name)
    return None

def enquire_queues_export(queue_list):
  logging.debug("Select Queues for export")
  questions = [
    inquirer.List('queues',
                      message="Select queues for export",
                      choices=queue_list,
                      ),
  ]
  return inquirer.prompt(questions)


def export_queue(vhost, queue_name):
  credentials = pika.PlainCredentials(cfg["username"], cfg["password"])
  parameters = pika.ConnectionParameters(cfg["host"],
                                       cfg["port"],
                                       vhost,
                                       credentials)
  logging.debug("exporting queue " + queue_name +" on vhost "+vhost)
  connection = pika.BlockingConnection(pika.ConnectionParameters(host=cfg["host"]))
  channel = connection.channel()
  channel.basic_consume(callback, queue=queue_name)
  logging.debug(' [*] Waiting for messages. To exit press CTRL+C')
  channel.start_consuming()
  

def callback(ch, method, properties, body):
  logging.debug(" [x] Received %r" % body)
  # ch.basic_ack(delivery_tag=method.delivery_tag)
  with open(cfg["exportOutput"], "a") as myfile:
    myfile.write("appended text")


  
def big_text(text):
    logging.info("\n"+figlet_format(text, font='starwars'))