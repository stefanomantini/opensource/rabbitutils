FROM python:3.6.6-alpine3.8

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./main.py" ]


#RUN pip install requests --no-cache-dir
#RUN pip install inquirer --no-cache-dir
#RUN pip install pyfiglet --no-cache-dir
#RUN pip install pika --no-cache-dir
#RUN mkdir /app
#ADD scripts /app/scripts
#ADD main.py /app
#ADD core_utils.py /app
#ADD config.json /app

#ENTRYPOINT ["python", "/app/main.py"]
